# Safety First
[![BuildStatus Widget]][BuildStatus Result]
[![CodeCov Widget]][CodeCov Result]
[![GoDoc Widget]][GoDoc Result]
[![GoReport Widget]][GoReport Status]

[BuildStatus Result]: https://travis-ci.org/philip-bui/safety-first
[BuildStatus Widget]: https://travis-ci.org/philip-bui/safety-first.svg?branch=master

[CodeCov Result]: https://codecov.io/gh/philip-bui/safety-first
[CodeCov Widget]: https://codecov.io/gh/philip-bui/safety-first/branch/master/graph/badge.svg

[GoDoc Result]: https://godoc.org/philip-bui/safety-first
[GoDoc Widget]: https://godoc.org/philip-bui/safety-first?status.svg

[GoReport Status]: https://goreportcard.com/report/github.com/philip-bui/safety-first
[GoReport Widget]: https://goreportcard.com/badge/github.com/philip-bui/safety-first

A script that reads off a Queue and checks if the content is safe through a variety of APIs.


